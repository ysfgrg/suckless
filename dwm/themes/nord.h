static const char col_bg[]          = "#2e3440";
static const char col_gray[]        = "#666666";
static const char col_white[]       = "#d8dee9";
static const char col_border[]      = "#81a1c1";
static const char *colors[][3]      = {
[SchemeNorm]     = { col_gray, col_bg,  col_bg },
[SchemeSel]      = { col_white, col_bg,  col_border  },
};
