static const char col_bg[]          = "#282828";
static const char col_gray[]        = "#666666";
static const char col_white[]       = "#f0f0f0";
static const char col_border[]      = "#7daea3";
static const char *colors[][3]      = {
[SchemeNorm]     = { col_gray, col_bg,  col_bg },
[SchemeSel]      = { col_white, col_bg,  col_border  },
};
