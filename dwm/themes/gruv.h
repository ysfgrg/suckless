static const char col_gray1[]       = "#282828";
static const char col_gray3[]       = "#d4be98";
static const char col_gray4[]       = "#ffffff";
static const char *colors[][3]      = {
[SchemeNorm]     = { col_gray3, col_gray1,  col_gray1 },
[SchemeSel]      = { col_gray4, col_gray1,   col_gray1  },
[SchemeStatus]   = { col_gray3, col_gray1,  "#000000" }, 
[SchemeTagsSel]  = { col_gray4, col_gray1,   "#000000" }, 
[SchemeTagsNorm] = { col_gray3, col_gray1,  "#000000" },
[SchemeInfoSel]  = { col_gray4, col_gray1,  "#000000" },
[SchemeInfoNorm] = { col_gray3, col_gray1,  "#000000" },
};
