static const char col_bg[]       = "#1f2227";
static const char col_gray3[]    = "#666666";
static const char col_gray4[]    = "#ffffff";
static const char *colors[][3]      = {
[SchemeNorm]     = { col_gray3, col_bg,  col_bg },
[SchemeSel]      = { col_gray4, col_bg,  col_bg  },
[SchemeStatus]   = { col_gray3, col_bg,  "#000000" }, 
[SchemeTagsSel]  = { col_gray4, col_bg,  "#000000" }, 
[SchemeTagsNorm] = { col_gray3, col_bg,  "#000000" },
[SchemeInfoSel]  = { col_gray4, col_bg,  "#000000" },
[SchemeInfoNorm] = { col_gray3, col_bg,  "#000000" },
};
