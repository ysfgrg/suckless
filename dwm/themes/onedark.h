static const char col_bg[]          = "#1f2227";
static const char col_gray[]        = "#666666";
static const char col_white[]       = "#ffffff";
static const char col_border[]      = "#5e81ac";
static const char *colors[][3]      = {
[SchemeNorm]     = { col_gray, col_bg,  col_bg },
[SchemeSel]      = { col_white, col_bg,  col_border  },
};
