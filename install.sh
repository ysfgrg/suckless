#/bin/bash

echo "Install dependencies"
if command -v pacman &> /dev/null
then
sudo pacman -S --noconfirm base-devel opendoas lm_sensors psmisc    
elif command -v xbps-install &> /dev/null
then
sudo -Syu xbps-install libXft-devel libX11-devel harfbuzz-devel libXext-devel libXrender-devel libXinerama-devel base-devel psmisc opendoas lm_sensors 
fi

git clone https://gitlab.com/ysfgrg/suckless.git ~/.config/suckless
cd ~/.config/suckless/dmenu
sudo make install
cd ../dwm
sudo make install
cd ../st
sudo make install 
cd ../slstatus
sudo make install
cd ../
ech "copying The Doas Config"
sudo cp -r doas.conf /etc/doas.conf
echo "Creating Login Manager Entry for DWM"
mkdir -p /usr/share/xsessions/
cd /usr/share/xsessions
sudo touch dwm.desktop
sudo chown $USER dwm.desktop
sudo cat > dwm.desktop <<EOF
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=the dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
EOF
