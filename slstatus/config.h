/* See LICENSE file for copyright and license details. */

/* interval between updates (in ms) */
const unsigned int interval = 1000;
static const char unknown_str[] = "n/a";

#define MAXLEN 2048

static const struct arg args[] = {
// UPD
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command,   "^c#abb2bf^%s",   "sb-update"           },

// TEMP
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command,   "^c#abb2bf^%s",   "sb-temp"           },

// UPT
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command,   "^c#abb2bf^%s",   "sb-uptime"},

// vol_perc
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command,   "^c#abb2bf^%s",   "sb-volume"           },

// CPU PREC
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command,   "^c#abb2bf^%s",   "sb-cpu"           },

// Memory
{ separator,   "^c#666666^ | ",  NULL           },
{ run_command, "^c#abb2bf^%s",   "sb-mem"           },

// Date
{ separator,	 "^c#666666^ | ",  NULL           },
{ run_command, "^c#abb2bf^%s",   "sb-time"           },
};
