static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#D8DEE9", "#2e3440" },
	[SchemeSel] = { "#D8DEE9", "#81a1c1" },
	[SchemeSelHighlight] = { "#ebcb8b", "#81a1c1" },
	[SchemeNormHighlight] = { "#ebcb8b", "#2e3440" },
	[SchemeOut] = { "#2e3440", "#2e3440" },
};


