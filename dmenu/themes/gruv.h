static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#f0f0f0", "#282828" },
	[SchemeSel] = { "#f0f0f0", "#7daea3" },
	[SchemeSelHighlight] = { "#d8a657", "#7daea3" },
	[SchemeNormHighlight] = { "#d8a657", "#282828" },
	[SchemeOut] = { "#282828", "#282828" },
};
